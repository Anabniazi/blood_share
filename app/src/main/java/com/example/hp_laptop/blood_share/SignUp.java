package com.example.hp_laptop.blood_share;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;

public class SignUp extends AppCompatActivity {

    EditText username;
    EditText password;
    EditText re_password;
    Button signup;
    Bitmap photo;
    de.hdodenhof.circleimageview.CircleImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        this.username=(EditText)findViewById(R.id.Username);
        this.password= (EditText)findViewById(R.id.Password);
        this.re_password=(EditText)findViewById(R.id.co_Password);
        this.signup=(Button)findViewById(R.id.Login);
        this.imageView= (de.hdodenhof.circleimageview.CircleImageView) findViewById(R.id.user_profile_pic);


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"select files"),456);
            }
        });

        this.signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!isEmailValid(username.getText().toString()))
                {
                    Toast.makeText(SignUp.this,"EMail is not valid",Toast.LENGTH_LONG).show();
                }
                else if(!password.getText().toString().equals(re_password.getText().toString()))
                {
                    Toast.makeText(SignUp.this,"password and confirm password doesnot match",Toast.LENGTH_LONG).show();
                }
                else
                {
                    Log.i("db","user");
                    User s = new User();
                    s.setImage("aaaa");
                    s.setPassword(password.getText().toString());
                    s.setUsername(username.getText().toString());
                    DatabaseReference database = FirebaseDatabase.getInstance().getReference("blood_share");
                    database.child("Users").setValue(s).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid)
                        {

                           // Log.i("task.isSuccessful()", aVoid.toString()+"");
                            Log.i("task.isSuccessful()","firestore");
                        }
                    }).addOnFailureListener(new OnFailureListener()
                    {
                        @Override
                        public void onFailure(@NonNull Exception e)
                        {
                            Log.i("task.isSuccessful()1",e.toString()+"");
                        }
                    });
                }



            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RESULT_OK)
        {
            if(requestCode==456)
            {
                try
                {
                    photo = MediaStore.Images.Media.getBitmap(this.getApplicationContext().getContentResolver(), data.getData());
                    imageView.setImageBitmap(photo);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
