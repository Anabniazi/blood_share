package com.example.hp_laptop.blood_share;

import android.content.Intent;
import android.location.LocationListener;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.scalified.fab.ActionButton;

import org.w3c.dom.Text;

import java.util.concurrent.TimeUnit;


public class MainActivity extends AppCompatActivity {
    FirebaseAuth auth; //for google login
    GoogleApiClient googleApi; //for getting images,email,name
    GoogleSignInOptions googleSignInOptions; //show all login accounts
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    Button phone_verify;
    TextView signup;
    TextView addl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        auth = FirebaseAuth.getInstance(); //initilizae firebaseuth class

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();   // this object helps in showing all google accounts so that user can login through anyone


        googleApi = new GoogleApiClient.Builder(MainActivity.this)
                .enableAutoManage(MainActivity.this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API,googleSignInOptions)
                .build();
   // this class calleg GoogleApiClient helps to bring data from google
        // wherne ever we have to use any service of google we always need this class bcs it connects our app with google.

        //Google Button
        com.google.android.gms.common.SignInButton signInButton = (com.google.android.gms.common.SignInButton) findViewById(R.id.google_button);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent GoogleActivity =Auth.GoogleSignInApi.getSignInIntent(googleApi);
                startActivityForResult(GoogleActivity,123 );
                Log.i("startActivityForResult","startActivityForResult");
            }

            });

        phone_verify =(Button) findViewById(R.id.phone_button);

        phone_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                startPhoneNumberVerification("+923350172904");

            }
        });

        signup=(TextView)findViewById(R.id.footer_Second_text);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this,SignUp.class);
                startActivity(intent);
            }
        });


        // callback functions for phone

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential)
            {
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e)
            {

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    //Toast.makeText(this,"",Toast.LENGTH_LONG);
                    Toast.makeText(MainActivity.this,"This number is not valid",Toast.LENGTH_LONG).show();
                } else if (e instanceof FirebaseTooManyRequestsException) {

                }
            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                Log.i("code_sent",s);
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        if(requestCode==123)
        {
            GoogleSignInResult googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if(googleSignInResult.isSuccess())
            {
                GoogleSignInAccount googlesigininaccount  = googleSignInResult.getSignInAccount();

                FirebaseAuthuser(googlesigininaccount);
            }
        }
    }

    public void FirebaseAuthuser(GoogleSignInAccount googleSignInAccount)
    {
        //Google verification
        final AuthCredential authCredential = GoogleAuthProvider.getCredential(googleSignInAccount.getIdToken(),null);


        auth.signInWithCredential(authCredential)
                .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {


                        if(task.isSuccessful())
                        {
                            FirebaseUser firebaseuser = auth.getCurrentUser();
                            firebaseuser.getDisplayName();
                            firebaseuser.getEmail();
                            firebaseuser.getPhotoUrl();
                        }
                    }
                });
    }


    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful())
                        {
                            Log.d("success", "signInWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();

                        }
                        else

                            {
                            Log.w("failure", "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                Toast.makeText(MainActivity.this,"User login has failed",Toast.LENGTH_LONG);
                            }
                        }
                    }
                });
    }
    private void startPhoneNumberVerification(String phoneNumber) {

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber, 60,  TimeUnit.SECONDS,   this,  mCallbacks);        }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void resendVerificationCode(String phoneNumber,PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber, 60,   TimeUnit.SECONDS,  this,  mCallbacks, token);      }
}